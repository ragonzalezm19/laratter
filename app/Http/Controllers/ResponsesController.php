<?php

namespace App\Http\Controllers;

use App\Response;
use Illuminate\Http\Request;

class ResponsesController extends Controller
{
    public function user(Response $response)
    {
        return $response->user;
    }
}
