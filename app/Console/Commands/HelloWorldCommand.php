<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class HelloWorldCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hello:world {argumento1 : Esto sera algo} {--algo : esto no hace nada}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este es nuestro Hola Mundo de artisan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $valor = $this->argument('argumento1');

        $this->line("Me pasaste el argumento $valor");

        $this->error('algo');
        $this->info('algo');
    }
}
